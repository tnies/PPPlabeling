# -*- coding: utf-8 -*-
"""
@author: tim
"""

import numpy as np
import scipy.integrate as scp
import modelbase
import matplotlib.pyplot as plt
import numdifftools as nd
import re


#BASIC MODEL FROM MCINTYRE ET AL. (1988)

class PPP(object):
    
    def __init__(self):
        
        #Parameters from McIntyre et al. 1988
        
        self.par= {'RU5PE_k1':3.91e6, 'RU5PE_k2': 438,'RU5PE_k3':305,'RU5PE_k4':1.49e6,'RU5PE_Et':4.22e-6,
                   'RIB5PI_k1':21600,'RIB5PI_k2':14.2,'RIB5PI_k3':33.3,'RIB5PI_k4':60900,'RIB5PI_Et':14.2e-6,
                   'TK1_k1':2.16e5,'TK1_k2':38,'TK1_k3':34,'TK1_k4':1.56e5,'TK1_k5':3.29e5,'TK1_k6':175,'TK1_k7':40,'TK1_k8':4.48e4,'TK1_Et':0.33e-6,
                   'TK2_k1':2.16e5,'TK2_k2':38,'TK2_k3':34,'TK2_k4':1.56e5,'TK2_k5':2.24e6,'TK2_k6':175,'TK2_k7':40,'TK2_k8':2.13e4,'TK2_Et':0.33e-6,
                   'TA_k1':5.8e5,'TA_k2':45.3,'TA_k3':16.3,'TA_k4':1.01e6,'TA_k5':4.9e5,'TA_k6':60,'TA_k7':17,'TA_k8':7.9e4,'TA_Et':0.69e-6,
                   'TIM_k1':3.7e7,'TIM_k2':1320,'TIM_k3':1.46e4,'TIM_k4':1.9e7,'TIM_Et':1.14e-6,
                   'AL2_k1':1.07e7, 'AL2_k2':233, 'AL2_k3':1900, 'AL2_k4':1.12e7, 'AL2_k5':70, 'AL2_k6':6.41e6, 'AL2_Et':0.37e-6,
                   'AL1_k1':8.47e6,'AL1_k2':151,'AL1_k3':117,'AL1_k4':8.43e5,'AL1_k5':70,'AL1_k6':6.41e6,'AL1_Et':0.37e-6,
                   'MI_k1':1.19e6,'MI_k2':800,'MI_k3':800,'MI_k4':1.23e6,'MI_Et':0.628e-9,
                   'GPI_k1':3.98e7,'GPI_k2':1290,'GPI_k3':1550,'GPI_k4':1.56e7,'GPI_Et':0.2e-6,
                   'PM_k1':4.2e6,'PM_k2':80.9,'PM_k3':242.6,'PM_k4':7.2e5,'PM_k5':1.3e7,'PM_k6':10,'PM_Et':0.178e-6,
                   'UDPGal1PT_k1':7.58e5, 'UDPGal1PT_k2':100, 'UDPGal1PT_k3':150, 'UDPGal1PT_k4':8.75e5, 'UDPGal1PT_k5':3.96e5, 'UDPGal1PT_k6':110, 'UDPGal1PT_k7':113, 'UDPGal1PT_k8':8.73e5, 'UDPGal1PT_Et':0.048e-6}
                   
                   
        #Parameters from Berthon1993
                   
        self.par2= {'RU5PE_k1':3.91e6, 'RU5PE_k2': 438,'RU5PE_k3':305,'RU5PE_k4':1.49e6,'RU5PE_Et':2.96e-6,
                   'RIB5PI_k1':7.24e4,'RIB5PI_k2':3.138e1,'RIB5PI_k3':33.3,'RIB5PI_k4':8.29e4,'RIB5PI_Et':6.9e-5,
                   'TK1_k1':2.16e5,'TK1_k2':5.5e1,'TK1_k3':7.7e1,'TK1_k4':9.0e6,'TK1_k5':3.95e5,'TK1_k6':2.53e2,'TK1_k7':7.7e1,'TK1_k8':1.95e3,'TK1_Et':1.7e-7,
                   'TK2_k1':2.16e5,'TK2_k2':5.5e1,'TK2_k3':7.7e1,'TK2_k4':9.0e6,'TK2_k5':8.96e7,'TK2_k6':2.53e2,'TK2_k7':7.7e1,'TK2_k8':3.08e4,'TK2_Et':1.7e-7,
                   'TA_k1':6.6e5,'TA_k2':45.3,'TA_k3':16.3,'TA_k4':5.0e6,'TA_k5':4.58e6,'TA_k6':60,'TA_k7':17,'TA_k8':1.66e5,'TA_Et':9.1e-7,
                   'TIM_k1':3.7e7,'TIM_k2':1320,'TIM_k3':1.46e4,'TIM_k4':1.9e7,'TIM_Et':7.96e-7,
                   'AL2_k1':1.07e7, 'AL2_k2':233, 'AL2_k3':1900, 'AL2_k4':2.8e7, 'AL2_k5':7.0e1, 'AL2_k6':6.41e6, 'AL2_Et':3.4e-7,
                   'AL1_k1':8.47e6,'AL1_k2':151,'AL1_k3':117,'AL1_k4':4.05e6,'AL1_k5':7.0e1,'AL1_k6':6.41e6,'AL1_Et':3.4e-7,
                   'MI_k1':1.19e6,'MI_k2':800,'MI_k3':800,'MI_k4':1.23e6,'MI_Et':1.7e-10,
                   'GPI_k1':3.98e7,'GPI_k2':1290,'GPI_k3':1550,'GPI_k4':1.56e7,'GPI_Et':1.4e-7,
                   'PM_k1':80.9,'PM_k2':4.2e6,'PM_k3':7.2e5,'PM_k4':2.426e2,'PM_k5':1.0e1,'PM_k6':1.3e7,'PM_Et':1.0e-8,
                   'UDPGal1PT_k1':7.58e5, 'UDPGal1PT_k2':100, 'UDPGal1PT_k3':150, 'UDPGal1PT_k4':8.75e5, 'UDPGal1PT_k5':3.96e5, 'UDPGal1PT_k6':110, 'UDPGal1PT_k7':113, 'UDPGal1PT_k8':8.73e5, 'UDPGal1PT_Et':3.36e-8}


    #calculated rate laws using the King-Altman-Algorithm. Rate law was given in the paper. A and C was calculated. Especially C was then checked looking into the paper from Cleland 1963 

    def ratelawA(self,S,P,k1,k2,k3,k4,Et):
        return Et*((k1*k3*S-k2*k4*P)/(k2+k3+k1*S+k4*P))
        
    def ratelawB(self,A,B,P,Q,k1,k2,k3,k4,k5,k6,k7,k8,Et):
        return Et*((k1*k3*k5*k7*A*B-k2*k4*k6*k8*P*Q)/(k1*k3*(k6+k7)*A+k5*k7*(k2+k3)*B+k2*k4*(k6+k7)*P+k6*k8*(k2+k3)*Q+k1*k5*(k3+k7)*A*B+k4*k8*(k2+k6)*P*Q+k5*k8*(k2+k3)*B*Q+k1*k4*(k6+k7)*A*P))
        
    def ratelawC(self,S,P,Q,k1,k2,k3,k4,k5,k6,Et):
        return Et*((k1*k3*k5*S-k2*k4*k6*P*Q)/((k2+k3)*k5+k1*(k3+k5)*S+k2*k4*P+(k2+k3)*k6*Q+k1*k4*S*P+k4*k6*P*Q))
    
   # def ratelawE(self,A,M,P,k1,k2,k3,k4,k5,k6,Et):
       # return Et*((k2*k4*k6*A*M-k1*k3*k6*P*M)/(k6*k1*M+k6*k4*M+k6*k2*A*M+k3*k6*P*M+k2*k5*A+k3*k5*P)) # not used anymore
    
    #specific rate laws
    
    def Ru5PE(self,S,P):
        return self.ratelawA(S,P,self.par['RU5PE_k1'],self.par['RU5PE_k2'],self.par['RU5PE_k3'],self.par['RU5PE_k4'],self.par['RU5PE_Et'])
    
    def RIB5PI(self,S,P):
        return self.ratelawA(S,P,self.par['RIB5PI_k1'],self.par['RIB5PI_k2'],self.par['RIB5PI_k3'],self.par['RIB5PI_k4'],self.par['RIB5PI_Et'])
    
    def TK1(self,A,B,P,Q):
        return self.ratelawB(A,B,P,Q,self.par['TK1_k1'],self.par['TK1_k2'],self.par['TK1_k3'],self.par['TK1_k4'],self.par['TK1_k5'],self.par['TK1_k6'],self.par['TK1_k7'],self.par['TK1_k8'],self.par['TK1_Et'])
    
    def TK2(self,A,B,P,Q):
        return self.ratelawB(A,B,P,Q,self.par['TK2_k1'],self.par['TK2_k2'],self.par['TK2_k3'],self.par['TK2_k4'],self.par['TK2_k5'],self.par['TK2_k6'],self.par['TK2_k7'],self.par['TK2_k8'],self.par['TK2_Et'])
    
    def TA(self,A,B,P,Q):
        return self.ratelawB(A,B,P,Q,self.par['TA_k1'],self.par['TA_k2'],self.par['TA_k3'],self.par['TA_k4'],self.par['TA_k5'],self.par['TA_k6'],self.par['TA_k7'],self.par['TA_k8'],self.par['TA_Et'])
    
    def TIM(self,S,P):
        return self.ratelawA(S,P,self.par['TIM_k1'],self.par['TIM_k2'],self.par['TIM_k3'],self.par['TIM_k4'],self.par['TIM_Et'])
    
    def AL1(self,S,P,Q):
        return self.ratelawC(S,P,Q,self.par['AL1_k1'],self.par['AL1_k2'],self.par['AL1_k3'],self.par['AL1_k4'],self.par['AL1_k5'],self.par['AL1_k6'],self.par['AL1_Et'])
    
    def AL2(self,S,P,Q):
        return self.ratelawC(S,P,Q,self.par['AL2_k1'],self.par['AL2_k2'],self.par['AL2_k3'],self.par['AL2_k4'],self.par['AL2_k5'],self.par['AL2_k6'],self.par['AL2_Et'])
    
    def MI(self,S,P):
        return self.ratelawA(S,P,self.par['MI_k1'],self.par['MI_k2'],self.par['MI_k3'], self.par['MI_k4'],self.par['MI_Et'])
    
    def GPI(self,S,P):
        return self.ratelawA(S,P,self.par['GPI_k1'],self.par['GPI_k2'],self.par['GPI_k3'], self.par['GPI_k4'],self.par['GPI_Et'])
        
    def PM(self,S,P):
        return self.ratelawA(S,P,self.par['PM_k1'],self.par['PM_k2'],self.par['PM_k3'],self.par['PM_k4'],self.par['PM_Et'])
        
    def UDPGal1PT(self,A,B,P,Q):
        return self.ratelawB(A,B,P,Q,self.par['UDPGal1PT_k1'],self.par['UDPGal1PT_k2'],self.par['UDPGal1PT_k3'],self.par['UDPGal1PT_k4'],self.par['UDPGal1PT_k5'],self.par['UDPGal1PT_k6'],self.par['UDPGal1PT_k7'],self.par['UDPGal1PT_k8'],self.par['UDPGal1PT_Et'])
    
    
      #Model
    
    def modell(self,T,x0):
        
        Ru5P=x0[0] 
        Xu5P=x0[1]
        Rib5P=x0[2]
        G3P=x0[3]
        Sed7P=x0[4]
        Ery4P=x0[5]
        Fru6P=x0[6]
        DHAP=x0[7]
        Sed1_7P2=x0[8]
        Fru1_6P2=x0[9]
        Man6P=x0[10]
        Glc6P=x0[11]
        Glc1P=x0[12]
        UDPGlc = x0[13]
        Gal1P =x0[14]
        UDPGal1P = x0[15]
        
        dRu5P=self.RIB5PI(Rib5P,Ru5P)-self.Ru5PE(Ru5P,Xu5P)
        
        dXu5P=self.Ru5PE(Ru5P,Xu5P)-self.TK1(Xu5P,Rib5P,G3P,Sed7P)-self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dRib5P=-self.RIB5PI(Rib5P,Ru5P)-self.TK1(Xu5P,Rib5P,G3P,Sed7P)
        
        dG3P=self.TK1(Xu5P,Rib5P,G3P,Sed7P)+self.AL2(Fru1_6P2,G3P,DHAP)-self.TA(Sed7P,G3P,Ery4P,Fru6P)-self.TIM(G3P,DHAP)+self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dSed7P=self.TK1(Xu5P,Rib5P,G3P,Sed7P)-self.TA(Sed7P,G3P,Ery4P,Fru6P)
        
        dEry4P=self.TA(Sed7P,G3P,Ery4P,Fru6P)+self.AL1(Sed1_7P2,Ery4P,DHAP)-self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dFru6P=self.TA(Sed7P,G3P,Ery4P,Fru6P)+self.MI(Man6P,Fru6P)+self.TK2(Xu5P,Ery4P,G3P,Fru6P)-self.GPI(Fru6P,Glc6P)
        
        dDHAP=self.AL1(Sed1_7P2,Ery4P,DHAP)+self.AL2(Fru1_6P2,G3P,DHAP)+self.TIM(G3P,DHAP)
        
        dSed1_7P2=-self.AL1(Sed1_7P2,Ery4P,DHAP)
        
        dFru1_6P2=-self.AL2(Fru1_6P2,G3P,DHAP)
        
        dMan6P=-self.MI(Man6P,Fru6P)
        
        dGlc6P=self.GPI(Fru6P,Glc6P)+self.PM(Glc1P,Glc6P)
        
        dGlc1P=-self.PM(Glc1P,Glc6P)-self.UDPGal1PT(UDPGlc,Glc1P,Gal1P,UDPGal1P)
        
        dUDPGlc = -self.UDPGal1PT(UDPGlc,Glc1P,Gal1P,UDPGal1P)
        
        dGal1P = self.UDPGal1PT(UDPGlc,Glc1P,Gal1P,UDPGal1P)
        
        dUDPGal1P = self.UDPGal1PT(UDPGlc,Glc1P,Gal1P,UDPGal1P)
        
        return [dRu5P,dXu5P,dRib5P,dG3P,dSed7P,dEry4P,dFru6P,dDHAP,dSed1_7P2,dFru1_6P2,dMan6P,dGlc6P,dGlc1P,dUDPGlc,dGal1P,dUDPGal1P]
    
    
    
    #FUNCTION FOR TIMEINTEGRATION
    def timeintegrator(self,x0,T):
        
        time = T
        results0 = [x0]

        integrator = scp.ode(self.modell).set_integrator('lsoda').set_initial_value(x0,0)   
        cnt = 1  


        while cnt < len(time):
            zahl = integrator.integrate(time[cnt])
            results0.append(zahl) 
            cnt += 1
        
        return(results0)
        
    #CALCULATES THE EQUILIBRIUM CONSTANTS GIVEN A STEADY STATE
    @classmethod    
    def calcKeq(cls,steadystate):
        names=['Ru5PE_K','RIB5PI_K','TK1_K','TK2_K','TA_K','TIM_K','AL1_K','AL2_K','MI_K','GPI_K','PM_K','UDPGAL1PT_K']
        basis=steadystate
        res=[]
        res.append(basis[1]/basis[0]) #Ru5PE
        res.append(basis[0]/basis[2]) #RIB5PI
        res.append((basis[3]*basis[4])/(basis[1]*basis[2])) #TK1
        res.append((basis[3]*basis[6])/(basis[1]*basis[5])) #TK2
        res.append((basis[5]*basis[6])/(basis[4]*basis[3])) #TA
        res.append(basis[7]/basis[3]) #TIM
        res.append((basis[5]*basis[7])/basis[8])#Al1
        res.append((basis[7]*basis[3])/basis[9])#AL2
        res.append(basis[6]/basis[10]) #MI
        res.append(basis[11]/basis[6]) #GPI
        res.append(basis[11]/basis[12])
        
        if cls != short_PPP:
            res.append((basis[15]*basis[14])/(basis[13]*basis[12]))
        
        print('---------------------------------------------'+'\n'+'\n'+'equilibriums constants:'+'\n')
    
        for i in range(len(res)):
            print('Keq_'+ names[i]+': ', res[i])

            
            
            
    #CALCULATES THE STEADY STATE: SOMETIMES VERY TIME CONSUMING       
    def steadystate(self,x0,toleranz=1e-25):
        labels = ['Ru5P','Xu5P','Rib5P','G3P','Sed7P','Ery4P','Fru6P','DHAP','Sed1_7P2','Fru1_6P2','Man6P','Glc6P','Glc1P','UDPGlc','Gal1P','UDPGal1P']
        time=range(1000000000)
        results0=[x0]
        error=np.linalg.norm(results0[0],ord=2)
        integrator=scp.ode(self.modell).set_integrator('lsoda').set_initial_value(x0,0)
        cnt=1
        
        while cnt < len(time) and error > toleranz:
            zahl = integrator.integrate(time[cnt])
            error=np.linalg.norm(zahl-results0[-1],ord=2)
            results0.append(zahl) 
            cnt += 1
            
#        print('time point of the steady_state:','\t',integrator.t)
#        print('\n','steady state:')
#        
#        for i in range(len(results0[-1])):
#            print('\n','-------------------------','\n',labels[i],':','\t',results0[-1][i])
        
        
        return results0[-1]
    
    
    def calculatePar(self,reactionName):
        
        ratelawDict = {'RU5PE':'A','RIB5PI':'A','TK1':'B','TK2':'B','TA':'B','TIM':'A',
                      'AL1':'C','AL2':'C','MI':'A','GPI':'A','PM':'A','UDPGAL1PT':'A'}
        CoefDict = {}
        if ratelawDict[reactionName]=='A':
            # GENERATE COEFFICIENT LABELS
            Coef = [] 
            numCoef = range(4)
            [Coef.append(reactionName+str('_')+'k'+str(i+1)) for i in numCoef]
            Coef.append(reactionName+str('_')+'Et')
            
            #CALCULATES MEASURABLE KINETIC CONSTANTS (CLELAND 1963)    
            CoefDict['Keq'] = (self.par[Coef[0]] * self.par[Coef[2]]) / (self.par[Coef[1]] * self.par[Coef[3]])
            CoefDict['kcat_f'] = (self.par[Coef[0]] * self.par[Coef[2]])/(self.par[Coef[0]]+self.par[Coef[1]]+self.par[Coef[2]])
            CoefDict['kcat_r'] = (self.par[Coef[1]] * self.par[Coef[3]])/(self.par[Coef[3]])
        elif ratelawDict[reactionName]=='B':
            Coef = [] 
            numCoef = range(8)
            [Coef.append(reactionName+str('_')+'k'+str(i+1)) for i in numCoef]
            Coef.append(reactionName+str('_')+'Et')
            CoefDict['Keq'] = (self.par[Coef[0]]*self.par[Coef[2]]*self.par[Coef[4]]*self.par[Coef[6]])/(self.par[Coef[1]]*self.par[Coef[3]]*self.par[Coef[5]]*self.par[Coef[7]])           
            CoefDict['kcat_f'] = (self.par[Coef[0]]*self.par[Coef[2]]*self.par[Coef[4]]*self.par[Coef[6]])/(self.par[Coef[0]]*self.par[Coef[4]]*(self.par[Coef[2]]+self.par[Coef[6]]))
            CoefDict['kcat_r'] = (self.par[Coef[1]]*self.par[Coef[3]]*self.par[Coef[5]]*self.par[Coef[7]])/(self.par[Coef[3]]*self.par[Coef[7]]*(self.par[Coef[1]]+self.par[Coef[5]]))
        elif ratelawDict[reactionName]=='C':
            Coef = [] 
            numCoef = range(6)
            [Coef.append(reactionName+str('_')+'k'+str(i+1)) for i in numCoef]
            Coef.append(reactionName+str('_')+'Et')
            CoefDict['Keq'] = (self.par[Coef[0]]*self.par[Coef[2]]*self.par[Coef[4]])/(self.par[Coef[1]]*self.par[Coef[3]]*self.par[Coef[5]])
            CoefDict['kcat_f'] = (self.par[Coef[0]]*self.par[Coef[2]]*self.par[Coef[4]])/(self.par[Coef[0]]*(self.par[Coef[2]]+self.par[Coef[4]]))
            CoefDict['kcat_r'] = self.par[Coef[1]]  
        else:
            print('Not good')
        return(CoefDict)
        
        
        
    def concentrationControlCoefficients(self,y0,pname,norm=True):
        
        origValue = self.par[pname]
        
        def fn(x):
            self.par[pname] = x
            return self.steadystate(y0)
        
        jac = nd.Jacobian(fn,step=origValue/100.)
        
        cc = np.array(jac(origValue))
        
        self.par[pname] = origValue
        
        if norm:
            ss = self.steadystate(y0)
            cc = origValue * cc / ss
        
        return cc
        
        
    def rates(self,y):
        Ru5P=y[0] 
        Xu5P=y[1]
        Rib5P=y[2]
        G3P=y[3]
        Sed7P=y[4]
        Ery4P=y[5]
        Fru6P=y[6]
        DHAP=y[7]
        Sed1_7P2=y[8]
        Fru1_6P2=y[9]
        Man6P=y[10]
        Glc6P=y[11]
        Glc1P=y[12]
       
      
        fluxDict={'v1':self.RIB5PI(Rib5P,Ru5P),
        'v2':self.Ru5PE(Ru5P,Xu5P),
        'v3':self.TK1(Xu5P,Rib5P,G3P,Sed7P),
        'v4':self.TK2(Xu5P,Ery4P,G3P,Fru6P),
        'v5': self.TA(Sed7P,G3P,Ery4P,Fru6P),
        'v6': self.AL1(Sed1_7P2,Ery4P,DHAP),
        'v7': self.AL2(Fru1_6P2,G3P,DHAP),
        'v8': self.GPI(Fru6P,Glc6P),
        'v9': self.PM(Glc1P,Glc6P),
        'v10': self.TIM(G3P,DHAP),
        'v11':self.MI(Man6P,Fru6P)}
        
        if len(y) < 13:
            UDPGlc = y[13]
            Gal1P =y[14]
            UDPGal1P = y[15]
            fluxList = list(fluxDict.values())
            fluxList.append(self.UDPGal1PT(UDPGlc,Glc1P,Gal1P,UDPGal1P))
            return(fluxList)
        
        else:
            return(list(fluxDict.values()))
    
    
    
#SHORTER MODEL; EXCLUDES URIDYLTRANSFERASE, SINCE IT IS NOT REASONABLE TO INCLUDE THIS REACTION FOR LABELING EXPERIMENTS    
class short_PPP(PPP):
    
    def __init__(self):
        super().__init__()
    
   
    #Model
    def modell(self,T,x0):
        
        Ru5P=x0[0] 
        Xu5P=x0[1]
        Rib5P=x0[2]
        G3P=x0[3]
        Sed7P=x0[4]
        Ery4P=x0[5]
        Fru6P=x0[6]
        DHAP=x0[7]
        Sed1_7P2=x0[8]
        Fru1_6P2=x0[9]
        Man6P=x0[10]
        Glc6P=x0[11]
        Glc1P=x0[12]

        
        dRu5P=self.RIB5PI(Rib5P,Ru5P)-self.Ru5PE(Ru5P,Xu5P)
        
        dXu5P=self.Ru5PE(Ru5P,Xu5P)-self.TK1(Xu5P,Rib5P,G3P,Sed7P)-self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dRib5P=-self.RIB5PI(Rib5P,Ru5P)-self.TK1(Xu5P,Rib5P,G3P,Sed7P)
        
        dG3P=self.TK1(Xu5P,Rib5P,G3P,Sed7P)+self.AL2(Fru1_6P2,G3P,DHAP)-self.TA(Sed7P,G3P,Ery4P,Fru6P)-self.TIM(G3P,DHAP)+self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dSed7P=self.TK1(Xu5P,Rib5P,G3P,Sed7P)-self.TA(Sed7P,G3P,Ery4P,Fru6P)
        
        dEry4P=self.TA(Sed7P,G3P,Ery4P,Fru6P)+self.AL1(Sed1_7P2,Ery4P,DHAP)-self.TK2(Xu5P,Ery4P,G3P,Fru6P)
        
        dFru6P=self.TA(Sed7P,G3P,Ery4P,Fru6P)+self.MI(Man6P,Fru6P)+self.TK2(Xu5P,Ery4P,G3P,Fru6P)-self.GPI(Fru6P,Glc6P)
        
        dDHAP=self.AL1(Sed1_7P2,Ery4P,DHAP)+self.AL2(Fru1_6P2,G3P,DHAP)+self.TIM(G3P,DHAP)
        
        dSed1_7P2=-self.AL1(Sed1_7P2,Ery4P,DHAP)
        
        dFru1_6P2=-self.AL2(Fru1_6P2,G3P,DHAP)
        
        dMan6P=-self.MI(Man6P,Fru6P)
        
        dGlc6P=self.GPI(Fru6P,Glc6P)+self.PM(Glc1P,Glc6P)
        
        dGlc1P=-self.PM(Glc1P,Glc6P)
        
        return [dRu5P,dXu5P,dRib5P,dG3P,dSed7P,dEry4P,dFru6P,dDHAP,dSed1_7P2,dFru1_6P2,dMan6P,dGlc6P,dGlc1P]
    
   

#BEGIN OF THE LABELMODEL. BASED ON THE SHORTER MODEL     
        
class LabelPPP(modelbase.LabelModel):
    
    def __init__(self):
    
        super().__init__()
        
        #GET THE ORIGINAL PARAMETER        
        PPPmodel = PPP()
        self.origPars = PPPmodel.par.copy()
#        
        #UPDATE PARAMETER OF THE LABELMODEL
        self.par.update(self.origPars)
#        
        # ADD BASIC COMPOUNDS
        self.add_base_cpd('Ru5P',5)
        self.add_base_cpd('Xu5P',5)
        self.add_base_cpd('Rib5P',5)
        self.add_base_cpd('G3P',3)
        self.add_base_cpd('Sed7P',7)
        self.add_base_cpd('Ery4P',4)
        self.add_base_cpd('Fru6P',6)
        self.add_base_cpd('DHAP',3)
        self.add_base_cpd('Sed1_7P2',7)
        self.add_base_cpd('Fru1_6P2',6)
        self.add_base_cpd('Man6P',6)
        self.add_base_cpd('Glc6P',6)
        self.add_base_cpd('Glc1P',6)

##        
#        
        
        # GENERAL RATE EQUATIONS
        def ratelawAf(S,Stot,Ptot,k1,k2,k3,k4,Et):
            return Et*((k1*k3*S)/(k2+k3+k1*Stot+k4*Ptot))
            
        def ratelawAr(P,Stot,Ptot,k1,k2,k3,k4,Et):
            return Et*((k2*k4*P)/(k2+k3+k1*Stot+k4*Ptot))
        
        def ratelawBf(A,B,Atot,Btot,Ptot,Qtot,k1,k2,k3,k4,k5,k6,k7,k8,Et):
            return Et*((k1*k3*k5*k7*A*B)/(k1*k3*(k6+k7)*Atot+k5*k7*(k2+k3)*Btot+k2*k4*(k6+k7)*Ptot+k6*k8*(k2+k3)*Qtot+k1*k5*(k3+k7)*Atot*Btot+k4*k8*(k2+k6)*Ptot*Qtot+k5*k8*(k2+k3)*Btot*Qtot+k1*k4*(k6+k7)*Atot*Ptot))
            
        def ratelawBr(P,Q,Atot,Btot,Ptot,Qtot,k1,k2,k3,k4,k5,k6,k7,k8,Et):
            return Et*((k2*k4*k6*k8*P*Q)/(k1*k3*(k6+k7)*Atot+k5*k7*(k2+k3)*Btot+k2*k4*(k6+k7)*Ptot+k6*k8*(k2+k3)*Qtot+k1*k5*(k3+k7)*Atot*Btot+k4*k8*(k2+k6)*Ptot*Qtot+k5*k8*(k2+k3)*Btot*Qtot+k1*k4*(k6+k7)*Atot*Ptot))
        
        def ratelawCf(S,Stot,Ptot,Qtot,k1,k2,k3,k4,k5,k6,Et):
            return Et*((k1*k3*k5*S)/((k2+k3)*k5+k1*(k3+k5)*Stot+k2*k4*Ptot+(k2+k3)*k6*Qtot+k1*k4*Stot*Ptot+k4*k6*Ptot*Qtot))
            
        def ratelawCr(P,Q,Stot,Ptot,Qtot,k1,k2,k3,k4,k5,k6,Et):
            return Et*((k2*k4*k6*P*Q)/((k2+k3)*k5+k1*(k3+k5)*Stot+k2*k4*Ptot+(k2+k3)*k6*Qtot+k1*k4*Stot*Ptot+k4*k6*Ptot*Qtot))
    
#    
#    
#    
#    
#        # SPECIFIC RATE EQUATIONS
#    
#    
        def Ru5PEf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.RU5PE_k1,p.RU5PE_k2,p.RU5PE_k3,p.RU5PE_k4,p.RU5PE_Et)
            
        self.add_carbonmap_reaction('Ru5PEf',Ru5PEf,[0,1,2,3,4],['Ru5P'],['Xu5P'],'Ru5P','Ru5P_total','Xu5P_total')
        
        
        
        def Ru5PEr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.RU5PE_k1,p.RU5PE_k2,p.RU5PE_k3,p.RU5PE_k4,p.RU5PE_Et)
            
        self.add_carbonmap_reaction('Ru5PEr',Ru5PEr,[0,1,2,3,4],['Xu5P'],['Ru5P'],'Xu5P','Ru5P_total','Xu5P_total')
                
        
        
        

        def RIB5PIf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.RIB5PI_k1,p.RIB5PI_k2,p.RIB5PI_k3,p.RIB5PI_k4,p.RIB5PI_Et)
            
        self.add_carbonmap_reaction('RIB5PIf',RIB5PIf,[0,1,2,3,4],['Rib5P'],['Ru5P'],'Rib5P','Rib5P_total','Ru5P_total')
        
        
        
        def RIB5PIr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.RIB5PI_k1,p.RIB5PI_k2,p.RIB5PI_k3,p.RIB5PI_k4,p.RIB5PI_Et)
            
        self.add_carbonmap_reaction('RIB5PIr',RIB5PIr,[0,1,2,3,4],['Ru5P'],['Rib5P'],'Ru5P','Rib5P_total','Ru5P_total')
        
        
        
        
        
        def TK1f(p,A,B,Atot,Btot,Ptot,Qtot):
            return ratelawBf(A,B,Atot,Btot,Ptot,Qtot,p.TK1_k1,p.TK1_k2,p.TK1_k3,p.TK1_k4,p.TK1_k5,p.TK1_k6,p.TK1_k7,p.TK1_k8,p.TK1_Et)
            
        self.add_carbonmap_reaction('TK1f',TK1f,[2,3,4,0,1,5,6,7,8,9],['Xu5P','Rib5P'],['G3P','Sed7P'],'Xu5P','Rib5P','Xu5P_total','Rib5P_total','G3P_total','Sed7P_total')
        
        
        
        def TK1r(p,P,Q,Atot,Btot,Ptot,Qtot):
            return ratelawBr(P,Q,Atot,Btot,Ptot,Qtot,p.TK1_k1,p.TK1_k2,p.TK1_k3,p.TK1_k4,p.TK1_k5,p.TK1_k6,p.TK1_k7,p.TK1_k8,p.TK1_Et)
            
        self.add_carbonmap_reaction('TK1r',TK1r,[3,4,0,1,2,5,6,7,8,9],['G3P','Sed7P'],['Xu5P','Rib5P'],'G3P','Sed7P','Xu5P_total','Rib5P_total','G3P_total','Sed7P_total')
        
        
        
        
        
        def TK2f(p,A,B,Atot,Btot,Ptot,Qtot):
            return ratelawBf(A,B,Atot,Btot,Ptot,Qtot,p.TK2_k1,p.TK2_k2,p.TK2_k3,p.TK2_k4,p.TK2_k5,p.TK2_k6,p.TK2_k7,p.TK2_k8,p.TK2_Et)
            
        self.add_carbonmap_reaction('TK2f',TK2f,[2,3,4,0,1,5,6,7,8],['Xu5P','Ery4P'],['G3P','Fru6P'],'Xu5P','Ery4P','Xu5P_total','Ery4P_total','G3P_total','Fru6P_total')
        
        
        
        def TK2r(p,P,Q,Atot,Btot,Ptot,Qtot):
            return ratelawBr(P,Q,Atot,Btot,Ptot,Qtot,p.TK2_k1,p.TK2_k2,p.TK2_k3,p.TK2_k4,p.TK2_k5,p.TK2_k6,p.TK2_k7,p.TK2_k8,p.TK2_Et)
            
        self.add_carbonmap_reaction('TK2r',TK2r,[3,4,0,1,2,5,6,7,8],['G3P','Fru6P'],['Xu5P','Ery4P'],'G3P','Fru6P','Xu5P_total','Ery4P_total','G3P_total','Fru6P_total')

        
        


        def TAf(p,A,B,Atot,Btot,Ptot,Qtot):
            return ratelawBf(A,B,Atot,Btot,Ptot,Qtot,p.TA_k1,p.TA_k2,p.TA_k3,p.TA_k4,p.TA_k5,p.TA_k6,p.TA_k7,p.TA_k8,p.TA_Et)
        
        self.add_carbonmap_reaction('TAf',TAf,[3,4,5,6,0,1,2,7,8,9],['Sed7P','G3P'],['Ery4P','Fru6P'],'Sed7P','G3P','Sed7P_total','G3P_total','Ery4P_total','Fru6P_total')    
        
        
        
        def TAr(p,P,Q,Atot,Btot,Ptot,Qtot):
            return ratelawBr(P,Q,Atot,Btot,Ptot,Qtot,p.TA_k1,p.TA_k2,p.TA_k3,p.TA_k4,p.TA_k5,p.TA_k6,p.TA_k7,p.TA_k8,p.TA_Et)
        
        self.add_carbonmap_reaction('TAr',TAr,[4,5,6,0,1,2,3,7,8,9],['Ery4P','Fru6P'],['Sed7P','G3P'],'Ery4P','Fru6P','Sed7P_total','G3P_total','Ery4P_total','Fru6P_total')    
       
        
        
        
        
        def TIMf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.TIM_k1,p.TIM_k2,p.TIM_k3,p.TIM_k4,p.TIM_Et)
            
        self.add_carbonmap_reaction('TIMf',TIMf,[2,1,0],['G3P'],['DHAP'],'G3P','G3P_total','DHAP_total')
        
        
        
        def TIMr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.TIM_k1,p.TIM_k2,p.TIM_k3,p.TIM_k4,p.TIM_Et)
            
        self.add_carbonmap_reaction('TIMr',TIMr,[2,1,0],['DHAP'],['G3P'],'DHAP','G3P_total','DHAP_total')
        
        
        
        
        
        def AL1f(p,S,Stot,Ptot,Qtot):
            return ratelawCf(S,Stot,Ptot,Qtot,p.AL1_k1,p.AL1_k2,p.AL1_k3,p.AL1_k4,p.AL1_k5,p.AL1_k6,p.AL1_Et)
        
        self.add_carbonmap_reaction('AL1f',AL1f,[3,4,5,6,0,1,2],['Sed1_7P2'],['Ery4P','DHAP'],'Sed1_7P2','Sed1_7P2_total','Ery4P_total','DHAP_total')    
        
        
        
        def AL1r(p,P,Q,Stot,Ptot,Qtot):
            return ratelawCr(P,Q,Stot,Ptot,Qtot,p.AL1_k1,p.AL1_k2,p.AL1_k3,p.AL1_k4,p.AL1_k5,p.AL1_k6,p.AL1_Et)
        
        self.add_carbonmap_reaction('AL1r',AL1r,[4,5,6,0,1,2,3],['Ery4P','DHAP'],['Sed1_7P2'],'Ery4P','DHAP','Sed1_7P2_total','Ery4P_total','DHAP_total')  
        
        
        
        
        
        def AL2f(p,S,Stot,Ptot,Qtot):
            return ratelawCf(S,Stot,Ptot,Qtot,p.AL2_k1,p.AL2_k2,p.AL2_k3,p.AL2_k4,p.AL2_k5,p.AL2_k6,p.AL2_Et)
        
        self.add_carbonmap_reaction('AL2f',AL2f,[3,4,5,0,1,2],['Fru1_6P2'],['G3P','DHAP'],'Fru1_6P2','Fru1_6P2_total','G3P_total','DHAP_total')
        
        
        
        def AL2r(p,P,Q,Stot,Ptot,Qtot):
            return ratelawCr(P,Q,Stot,Ptot,Qtot,p.AL2_k1,p.AL2_k2,p.AL2_k3,p.AL2_k4,p.AL2_k5,p.AL2_k6,p.AL2_Et)
        
        self.add_carbonmap_reaction('AL2r',AL2r,[3,4,5,0,1,2],['G3P','DHAP'],['Fru1_6P2'],'G3P','DHAP','Fru1_6P2_total','G3P_total','DHAP_total')
          
          
          
          
          
        def MIf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.MI_k1,p.MI_k2,p.MI_k3, p.MI_k4,p.MI_Et)
            
        self.add_carbonmap_reaction('MIf',MIf,[0,1,2,3,4,5],['Man6P'],['Fru6P'],'Man6P','Man6P_total','Fru6P_total')
        
        
        
        def MIr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.MI_k1,p.MI_k2,p.MI_k3, p.MI_k4,p.MI_Et)
            
        self.add_carbonmap_reaction('MIr',MIr,[0,1,2,3,4,5],['Fru6P'],['Man6P'],'Fru6P','Man6P_total','Fru6P_total')
        
        
        
        
        
        def GPIf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.GPI_k1,p.GPI_k2,p.GPI_k3, p.GPI_k4,p.GPI_Et)
            
        self.add_carbonmap_reaction('GPIf',GPIf,[0,1,2,3,4,5],['Fru6P'],['Glc6P'],'Fru6P','Fru6P_total','Glc6P_total')
        
        
                
        def GPIr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.GPI_k1,p.GPI_k2,p.GPI_k3, p.GPI_k4,p.GPI_Et)
            
        self.add_carbonmap_reaction('GPIr',GPIr,[0,1,2,3,4,5],['Glc6P'],['Fru6P'],'Glc6P','Fru6P_total','Glc6P_total')
        
        
        
            
                
        def PMf(p,S,Stot,Ptot):
            return ratelawAf(S,Stot,Ptot,p.PM_k1,p.PM_k2,p.PM_k3,p.PM_k4,p.PM_Et)
            
        self.add_carbonmap_reaction('PMf',PMf,[0,1,2,3,4,5],['Glc1P'],['Glc6P'],'Glc1P','Glc1P_total','Glc6P_total')
        
        
        
        def PMr(p,P,Stot,Ptot):
            return ratelawAr(P,Stot,Ptot,p.PM_k1,p.PM_k2,p.PM_k3,p.PM_k4,p.PM_Et)
            
        self.add_carbonmap_reaction('PMr',PMr,[0,1,2,3,4,5],['Glc6P'],['Glc1P'],'Glc6P','Glc1P_total','Glc6P_total')
        
        
        
        
        
            
            
    @staticmethod        
    def plot_isotopomers(name,s, plt_type=None, position = None):
        'Plot methods for Isotopmers: default---plot all labels of one metabolite in one plot. plt_type= all_in_one---plot all labels in one plot\
        plt_type = single --- plot each label of metabolite in an own plot'
        
        carbondict=s.model.cpdBaseNames
        
        if plt_type == None:    
            plt.figure()
            ax = plt.axes()
            plt.plot(s.getT(),np.vstack([s.getLabelAtPos(name,i) for i in range(carbondict[name])]).transpose(), linewidth =3)
            plt.legend([str(i+1) + '. C-atom' for i in range(carbondict[name])], fontsize = 15)
            plt.title(name + ' label', size=20)
            plt.xlabel('time in [s]', size=20)
            plt.xticks(fontsize=20)
            plt.ylabel('concentration [M]', size=20)
            plt.yticks(fontsize=20)
            ax.yaxis.offsetText.set_fontsize(20)
            plt.show()
            
        elif plt_type=='all_in_one':
            fig = plt.figure()
            ax = plt.subplot(111)
            for j in carbondict.keys():
                for i in range(carbondict[j]):
                    ax.plot(s.getT(),s.getLabelAtPos(j,i), label = str(j) + '-C'+str(i+1),)      
            box = ax.get_position()
            ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.3),fancybox=True, ncol=5)
            ax.set_xlabel('time in [s]')
            ax.set_ylabel('concentration [M]')
            plt.title('Labels')
            plt.show()
                
            
        elif plt_type=='single' and position == None: 
            for i in range(carbondict[name]):
                plt.figure()
                ax = plt.axes()
                plt.plot(s.getT(),s.getLabelAtPos(name,i))
                plt.title(name + '-C'+str(i+1)+'-label', size=15.8)
                plt.xlabel('time in [s]', size=15.8)
                plt.xticks(fontsize=15.8)
                plt.ylabel('concentration [M]', size=15.8)
                plt.yticks(fontsize=15.8)
                ax.yaxis.offsetText.set_fontsize(15.8)
                plt.show()
                
        elif plt_type == 'single' and type(position) == int:
            plt.figure()
            ax = plt.axes
            plt.plot(s.getT(),s.getLabelAtPos(name,position))
            plt.xlabel('time in [s]')
            plt.ylabel('concentration [M]')
            plt.title('Label at Position C-'+str(position+1))
            plt.show()
    
    
    def getIsotopomers(self,s,cpdName,all_iso=True,threshold = 1e-11):
        "get all isotopomers concentration vectors of a specified metabolite (cpdName)"
        
        isotopomers = []
        c = s.model.cpdBaseNames[cpdName]
        regexp = "\A" + cpdName + '.'*c + "\Z"
        argids = s.model.find_re_argids(regexp)
        IDs = self.cpdIds()
        for i in argids:
            name = [key for key, value in IDs.items() if value == i]
            Y = s.getVarsByName(name)
            spl_name = name[0].split('P')[1]
            if spl_name != '_total':
                if spl_name != '2_total':
                    isotopomers.append((spl_name,Y))
#        
        if all_iso == False:
            a,b = zip(*isotopomers)
            isotopomers = []
            a = list(a)
            b = list(b)
            for i in range(len(b)):
                if sum(b[i]) !=0:
                    isotopomers.append((a[i],b[i]))
            return(isotopomers)
        else:
            return(isotopomers)
            
        



            

            
        
    
    
        
        
   
    
