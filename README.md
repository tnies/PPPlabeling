# An Investigation of the Dynamic Isotope Distribution in the Non-oxidative Pentose Phsophate Pathway

This git repository contains the python 3 (recommended 3.6) implementation of the Bachelor's thesis entitled:

## "An Investigation of the Dynamic Isotope Distribution in the Non-oxidative Pentose Phsophate Pathway"

This work was supervised by Prof. Dr. Oliver Ebenhöh in the Institute for Theoretical and Quantitative Biology at the Heinrich-Heine University Düsseldorf.


### Contact:

Quantitative und Theoretische Biologie 

Heinrich-Heine-Universität Düsseldorf 

Building: 25.32.03.24
 
Universitätsstraße 1
 
40225 Düsseldorf 

