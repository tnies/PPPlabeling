# -*- coding: utf-8 -*-
"""
@author: tim
"""

"""This script is an attempt to simulate labelings in the pentose phosphate pathway"""


# load packages 
import modelbase
import numpy as np
from PPPmodels import LabelPPP
from PPPmodels import PPP

Test = LabelPPP()


#%% 

'Experiment 1 : start label: fully labeled Ru5P \
                            initial values and parameters specified by McIntyre et al. 1989'
                            
               
               
y0d ={'Ru5P':1e-7,
'Xu5P':1e-7,
'Rib5P':1e-7,
'G3P':1e-7,
'Sed7P':1e-7,
'Ery4P':1e-7,
'Fru6P':10e-6,
'DHAP':10e-6,
'Sed1_7P2':1e-7,
'Fru1_6P2':2e-6,
'Man6P':1e-7,
'Glc6P':30e-6,
'Glc1P':1e-7}  

y0 = Test.set_initconc_cpd_labelpos(y0d)
y0[Test.cpdIds()['Ru5P00000']]=0
y0[Test.cpdIds()['Ru5P11111']]=1e-7

# standard            
begin=0
end=100
steps=10000
T = np.linspace(begin,end,steps)
print('Begin %f\n End %f\n Steps %f' % (begin,end,steps ))

s = modelbase.Simulator(Test)
s.integrator.linear_solver = 'SPGMR'
s.integrator.atol = 1e-15
s.integrator.rtol = 1e-15

import time 
start_time = time.time()
s.timeCourse(T,y0)
print("--- %s seconds ---" % 
(time.time() - start_time))


Test.plot_isotopomers('Glc6P',s)


#%%
    

'Experiment 2: start label: fully labeled Ru5P \
                initial values and parameters specified by Berthon et al. 1993 (par2)'
            
Test = LabelPPP()
PPP_model = PPP()

y0d ={'Ru5P':1e-7,
'Xu5P':1e-7,
'Rib5P':1e-7,
'G3P':1e-7,
'Sed7P':1e-7,
'Ery4P':1e-7,
'Fru6P':5e-6,
'DHAP':5e-6,
'Sed1_7P2':1e-7,
'Fru1_6P2':28.5e-3,
'Man6P':1e-7,
'Glc6P':19e-3,
'Glc1P':1e-7} 

y0 = Test.set_initconc_cpd_labelpos(y0d)
y0[Test.cpdIds()['Ru5P00000']]=0
y0[Test.cpdIds()['Ru5P11111']]=1e-7


Test.par.update(PPP_model.par2)

print(Test.par.RU5PE_Et)

begin=0
end=100000
steps=10000

T = np.linspace(begin,end,steps)

####Normal####

s = modelbase.Simulator(Test)
s.integrator.linear_solver = 'SPGMR'
s.integrator.atol = 1e-15
s.integrator.rtol = 1e-15

import time 
start_time = time.time()
s.timeCourse(T,y0)
print("--- %s seconds ---" % 
(time.time() - start_time))


Test.plot_isotopomers('Glc6P',s)

s.storeResults('all_Ru5P_SPGMR_100000_B') 


#%%
#
#
'Effect of differently labeled Ribulose-5-phosphate on Glucose-6-phosphate'


Test = LabelPPP()
PPP_model = PPP()


Test.par.update(PPP_model.par2)

begin=0
end=1000
steps=10000

T = np.linspace(begin,end,steps)


for i in [0,1,2,3,4]:
    print('Now', i)
    y0 = Test.set_initconc_cpd_labelpos(y0d, labelpos={'Ru5P':i})
    Test.par.update(PPP_model.par2)  
    s = modelbase.Simulator(Test)
    s.integrator.linear_solver = 'SPGMR'
    s.integrator.atol = 1e-15
    s.integrator.rtol = 1e-15
    s.timeCourse(T,y0)
    s.storeResults('dlabRu5P_SPGMR_100000_B' + '_C' + str(i)) 
    Test.plot_isotopomers('Glc6P',s, plt_type='single')

#%%
    
'gene knockout experiment'
    
Test = LabelPPP()
PPP_model = PPP()

y0d ={'Ru5P':1e-7,
'Xu5P':1e-7,
'Rib5P':1e-7,
'G3P':1e-7,
'Sed7P':1e-7,
'Ery4P':1e-7,
'Fru6P':5e-6,
'DHAP':5e-6,
'Sed1_7P2':1e-7,
'Fru1_6P2':28.5e-3,
'Man6P':1e-7,
'Glc6P':19e-3,
'Glc1P':1e-7} 

y0 = Test.set_initconc_cpd_labelpos(y0d)
y0[Test.cpdIds()['Ru5P00000']]=0
y0[Test.cpdIds()['Ru5P11111']]=1e-7


Test.par.update(PPP_model.par2)

begin=0
end=100
steps=10000

T = np.linspace(begin,end,steps)

## TA deficit ##

s = modelbase.Simulator(Test)
s.integrator.linear_solver = 'SPGMR'
s.integrator.atol = 1e-15
s.integrator.rtol = 1e-15

Test.par.update({'TA_Et':0})

import time 
start_time = time.time()
s.timeCourse(T,y0)
print("--- %s seconds ---" % 
(time.time() - start_time))

s.storeResults('all_Ru5P_SPGMR_100_B2_TA') 

## TK deficit ##

s = modelbase.Simulator(Test)
s.integrator.linear_solver = 'SPGMR'
s.integrator.atol = 1e-15
s.integrator.rtol = 1e-15

Test.par.update({'TK1_Et':0})
Test.par.update({'TK2_Et':0})

import time 
start_time = time.time()
s.timeCourse(T,y0)
print("--- %s seconds ---" % 
(time.time() - start_time))

s.storeResults('all_Ru5P_SPGMR_100_B2_TK1Tk2') 
    
    
    
    