"Plot: Amount of possible isotopomers in a system, where each atom in the ctm \
potentially labeled"

print('execute labeling.py script before starting IsoAmountPlot.py')

import numpy as np
import matplotlib.pyplot as plt
import modelbase
from PPPmodels import LabelPPP

#Possible isotopomers

possibleIsotopomers = [('Glc1P', 64), ('Xu5P', 32), ('G3P', 8), ('Ru5P', 32), ('Ery4P', 16), ('Sed7P', 128), ('Man6P', 64), ('Glc6P', 64), ('Rib5P', 32), ('Sed1_7P2', 128), ('Fru1_6P2', 64), ('Fru6P', 64), ('DHAP', 8)]
possibleIsotopomers=sorted(possibleIsotopomers, key = lambda x: x[1])

Labelmodel = LabelPPP()
Sim = modelbase.Simulator(Labelmodel)
Sim.loadResults('./all_Ru5P_SPGMR_100000_B')


def IsoCounter(results):
    cpdList = list(Labelmodel.cpdBaseNames.keys())
    Counter = []
    for i in cpdList:
        a = Labelmodel.getIsotopomers(results,i,all_iso = False)
        Counter.append((i,len(a)))
    return Counter
    
SimIsotopomers = IsoCounter(Sim)

print(SimIsotopomers)

#SimIsotopomers =[('Ru5P', 8),
# ('Ery4P', 4),
# ('Man6P', 16),
# ('Glc6P', 16),
# ('Sed1_7P2', 8),
# ('Sed7P', 32),
# ('Glc1P', 16),
# ('G3P', 2),
# ('Rib5P', 8),
# ('Xu5P', 8),
# ('Fru1_6P2', 4),
# ('DHAP', 2),
# ('Fru6P', 16)]


#%%

sortedSimIsotopomers = []
sortList = list(zip(*possibleIsotopomers))

for i in sortList[0]:
    for l in SimIsotopomers:
        if l[0] == i:
            sortedSimIsotopomers.append(l)
#%%

names,posValue=list(zip(*possibleIsotopomers))
names,simValue = list(zip(*sortedSimIsotopomers))
plt.bar(range(len(names)),posValue, label = 'possible Isotopomers')
plt.bar(range(len(names)),simValue, label = 'simulated Isotopomers')
plt.xticks(range(len(names)),names, rotation='vertical',size=14)
plt.yticks(size=15)
plt.ylabel('number of Isotopomers',size=15)
plt.legend(fontsize=15)
plt.show()

#%%

np.array(simValue)/np.array(posValue) *100


#%%
# Figure 4.11: gene knockout experiments, see labeling.py

Labelmodel = LabelPPP()
SimTA = modelbase.Simulator(Labelmodel)
SimTA.loadResults('./all_Ru5P_SPGMR_100_B2_TA')
SimIsotopomersTA = IsoCounter(SimTA)
keyTA,valueTA = zip(*SimIsotopomersTA)

Labelmodel = LabelPPP()
SimTK = modelbase.Simulator(Labelmodel)
SimTK.loadResults('./all_Ru5P_SPGMR_100_B2_TK1Tk2')
SimIsotopomersTK = IsoCounter(SimTK)
keyTK,valueTK = zip(*SimIsotopomersTK)


plt.bar(range(2),[sum(valueTK)*100/704,sum(valueTA)*100/704])
plt.xticks(range(2),['TK','TA'])
plt.ylabel('percent of all possible isotopomers')