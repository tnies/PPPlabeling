# -*- coding: utf-8 -*-

"""
@author: tim

DESCRIPTION: script calculates concentration control coefficients
"""



import numpy as np
from PPPmodels import short_PPP
import collections

#%%


PPP_test = short_PPP()

y0d =collections.OrderedDict({'Ru5P':1e-7,
'Xu5P':1e-7,
'Rib5P':1e-3,
'G3P':1e-7,
'Sed7P':1e-7,
'Ery4P':1e-7,
'Fru6P':10e-6,
'DHAP':10e-6,
'Sed1_7P2':1e-7,
'Fru1_6P2':2e-6,
'Man6P':1e-7,
'Glc6P':30e-6,
'Glc1P':1e-7})

y0 = list(y0d.values())

steady = PPP_test.steadystate(y0)




#%%


def ccontrolc(y0,get_Coef = False):
    cont = []
    Coef = []
    
    ratelawDict = {'RU5PE':'A','RIB5PI':'A','TK1':'B','TK2':'B','TA':'B','TIM':'A',
                   'AL1':'C','AL2':'C','MI':'A','GPI':'A','PM':'A'}
                   
    
    for i in ratelawDict:
        
        if ratelawDict[i] == 'A':
            numCoef = range(4)
            [Coef.append(str(i)+'_k'+str(j+1)) for j in numCoef]
        if ratelawDict[i] == 'B':
            numCoef = range(8)
            [Coef.append(str(i)+'_k'+str(j+1)) for j in numCoef]      
        if ratelawDict[i] == 'C':
            numCoef = range(6)
            [Coef.append(str(i)+'_k'+str(j+1)) for j in numCoef]
 
            
    if get_Coef == False:
        for l in Coef:
            cont.append(PPP_test.concentrationControlCoefficients(y0,l))
            print(l)         
        return cont
        
    else:
        return Coef

#%%

G = ccontrolc(y0,True)
H = ['Ru5P','Xu5P',
'Rib5P',
'G3P',
'Sed7P',
'Ery4P',
'Fru6P',
'DHAP',
'Sed1_7P2',
'Fru1_6P2',
'Man6P',
'Glc6P',
'Glc1P']

#%%

B = ccontrolc(y0)

#%%

B = np.array(B)

print(sum(B[:,]))

#%%

array = B[:,0]

import matplotlib.pyplot as plt

fig, axis = plt.subplots()
heatmap=axis.pcolor(array, cmap=plt.cm.jet)
        
axis.set_yticks(np.arange(array.shape[0])+0.5, minor=False)
axis.set_xticks(np.arange(array.shape[1])+0.5, minor=False)
        
axis.set_yticklabels(G, minor = False)
axis.set_xticklabels(H,minor=False)
        
axis.set_xlabel('Affected Concentration')
axis.set_ylabel('Changed Parameter')
axis.tick_params(labelsize=12.5)
plt.xticks(rotation=90)
        
plt.colorbar(heatmap)
fig.set_size_inches(6.,10.)
plt.rcParams.update({'font.size':10})
plt.title('Concentration Control Coefficients')
plt.tight_layout()

plt.show()
