# -*- coding: utf-8 -*-
"""
@author: tim
"""
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages



#CREATE THE GRAPH

ctm = nx.Graph()
ctm.name = 'carbon transition map'
carbonDict={'Ru5P':5,'Xu5P':5,'Rib5P':5,'G3P':3,'Sed7P':7,
            'Ery4P':4,'Fru6P':6,'DHAP':3,'Sed1_7P2':7,'Fru1_6P2':6,
            'Man6P':6,'Glc6P':6,'Glc1P':6}
            
carbonNames = list(carbonDict.keys())

for i in carbonNames:
    for j in range(carbonDict[i]):
        ctm.add_node(i+ '-C' + str(j+1))
        
transitionList = [('Ru5P-C1','Xu5P-C1'),('Ru5P-C2','Xu5P-C2'),('Ru5P-C3','Xu5P-C3'),('Ru5P-C4','Xu5P-C4'),('Ru5P-C5','Xu5P-C5'),('Ru5P-C1','Rib5P-C1'),('Ru5P-C2','Rib5P-C2'),('Ru5P-C3','Rib5P-C3'),('Ru5P-C4','Rib5P-C4'),('Ru5P-C5','Rib5P-C5'),
                  ('Rib5P-C1','Sed7P-C3'),('Rib5P-C2','Sed7P-C4'),('Rib5P-C3','Sed7P-C5'),('Rib5P-C4','Sed7P-C6'),('Rib5P-C5','Sed7P-C7'),
                  ('Sed7P-C1','Xu5P-C1'),('Sed7P-C2','Xu5P-C2'),('Xu5P-C1','Fru6P-C1'),('Xu5P-C2','Fru6P-C2'),('Xu5P-C3','G3P-C1'),('Xu5P-C4','G3P-C2'),('Xu5P-C5','G3P-C3'),
                  ('Fru6P-C1','Sed7P-C1'),('Fru6P-C2','Sed7P-C2'),('Sed7P-C3','Fru6P-C3'),('Sed7P-C4','Ery4P-C1'),('Sed7P-C5','Ery4P-C2'),('Sed7P-C6','Ery4P-C3'),('Sed7P-C7','Ery4P-C4'),
                  ('Ery4P-C1','Sed1_7P2-C4'),('Ery4P-C2','Sed1_7P2-C5'),('Ery4P-C3','Sed1_7P2-C6'),('Ery4P-C4','Sed1_7P2-C7'),('Ery4P-C1','Fru6P-C3'),('Ery4P-C2','Fru6P-C4'),('Ery4P-C3','Fru6P-C5'),('Ery4P-C4','Fru6P-C6'),
                  ('Sed1_7P2-C1','DHAP-C1'),('Sed1_7P2-C2','DHAP-C2'),('Sed1_7P2-C3','DHAP-C3'),
                  ('DHAP-C1','Fru1_6P2-C1'),('DHAP-C2','Fru1_6P2-C2'),('DHAP-C3','Fru1_6P2-C3'),
                  ('G3P-C1','Fru1_6P2-C4'),('G3P-C2','Fru1_6P2-C5'),('G3P-C3','Fru1_6P2-C6'),('G3P-C1','DHAP-C3'),('G3P-C2','DHAP-C2'),('G3P-C3','DHAP-C1'),('G3P-C1','Fru6P-C4'),('G3P-C2','Fru6P-C5'),('G3P-C3','Fru6P-C6'),
                  ('Fru6P-C1','Glc6P-C1'),('Fru6P-C2','Glc6P-C2'),('Fru6P-C3','Glc6P-C3'),('Fru6P-C4','Glc6P-C4'),('Fru6P-C5','Glc6P-C5'),('Fru6P-C6','Glc6P-C6'),
                  ('Glc6P-C1','Glc1P-C1'),('Glc6P-C2','Glc1P-C2'),('Glc6P-C3','Glc1P-C3'),('Glc6P-C4','Glc1P-C4'),('Glc6P-C5','Glc1P-C5'),('Glc6P-C6','Glc1P-C6'),
                  ('Fru6P-C1','Man6P-C1'),('Fru6P-C2','Man6P-C2'),('Fru6P-C3','Man6P-C3'),('Fru6P-C4','Man6P-C4'),('Fru6P-C5','Man6P-C5'),('Fru6P-C6','Man6P-C6')]
                  
ctm.add_edges_from(transitionList)


attrs = {('Ru5P-C1','Xu5P-C1'):{'Enzyme':'RU5PE'}, 
         ('Ru5P-C2','Xu5P-C2'):{'Enzyme':'RU5PE'},
         ('Ru5P-C3','Xu5P-C3'):{'Enzyme':'RU5PE'},
         ('Ru5P-C4','Xu5P-C4'):{'Enzyme':'RU5PE'},
         ('Ru5P-C5','Xu5P-C5'):{'Enzyme':'RU5PE'},
         ('Ru5P-C1','Rib5P-C1'):{'Enzyme':'RIB5PI'},
         ('Ru5P-C2','Rib5P-C2'):{'Enzyme':'RIB5PI'},
         ('Ru5P-C3','Rib5P-C3'):{'Enzyme':'RIB5PI'},
         ('Ru5P-C4','Rib5P-C4'):{'Enzyme':'RIB5PI'},
         ('Ru5P-C5','Rib5P-C5'):{'Enzyme':'RIB5PI'},
         ('Rib5P-C1','Sed7P-C3'):{'Enzyme':'TK1'},
         ('Rib5P-C2','Sed7P-C4'):{'Enzyme':'TK1'},
         ('Rib5P-C3','Sed7P-C5'):{'Enzyme':'TK1'},
         ('Rib5P-C4','Sed7P-C6'):{'Enzyme':'TK1'},
         ('Rib5P-C5','Sed7P-C7'):{'Enzyme':'TK1'},
         ('Fru6P-C1','Sed7P-C1'):{'Enzyme':'TA'},
         ('Fru6P-C2','Sed7P-C2'):{'Enzyme':'TA'},
         ('Sed7P-C3','Fru6P-C3'):{'Enzyme':'TA'},
         ('Sed7P-C4','Ery4P-C1'):{'Enzyme':'TA'},
         ('Sed7P-C5','Ery4P-C2'):{'Enzyme':'TA'},
         ('Sed7P-C6','Ery4P-C3'):{'Enzyme':'TA'},
         ('Sed7P-C7','Ery4P-C4'):{'Enzyme':'TA'},
         ('Sed7P-C1','Xu5P-C1'):{'Enzyme':'TK1'},
         ('Sed7P-C2','Xu5P-C2'):{'Enzyme':'TK1'},
         ('Xu5P-C1','Fru6P-C1'):{'Enzyme':'TK2'},
         ('Xu5P-C2','Fru6P-C2'):{'Enzyme':'TK2'},
         ('Xu5P-C3','G3P-C1'):{'Enzyme1':'TK1','Enzyme2':'TK2'},
         ('Xu5P-C4','G3P-C2'): {'Enzyme1':'TK1','Enzyme2':'TK2'},
         ('Xu5P-C5','G3P-C3'):{'Enzyme1':'TK1','Enzyme2':'TK2'},
         ('Ery4P-C1','Sed1_7P2-C4'):{'Enzyme':'AL_S'},
         ('Ery4P-C2','Sed1_7P2-C5'):{'Enzyme':'AL_S'},
         ('Ery4P-C3','Sed1_7P2-C6'):{'Enzyme':'AL_S'},
         ('Ery4P-C4','Sed1_7P2-C7'):{'Enzyme':'AL_S'},
         ('Ery4P-C1','Fru6P-C3'):{'Enzyme':'TK2'},
         ('Ery4P-C2','Fru6P-C4'):{'Enzyme':'TK2'},
         ('Ery4P-C3','Fru6P-C5'):{'Enzyme':'TK2'},
         ('Ery4P-C4','Fru6P-C6'):{'Enzyme':'TK2'},
         ('Sed1_7P2-C1','DHAP-C1'):{'Enzyme':'AL_S'},
         ('Sed1_7P2-C2','DHAP-C2'):{'Enzyme':'AL_S'},
         ('Sed1_7P2-C3','DHAP-C3'):{'Enzyme':'AL_S'},
         ('DHAP-C1','Fru1_6P2-C1'):{'Enzyme':'AL_F'},
         ('DHAP-C2','Fru1_6P2-C2'):{'Enzyme':'AL_F'},
         ('DHAP-C3','Fru1_6P2-C3'):{'Enzyme':'AL_F'},
         ('G3P-C1','Fru1_6P2-C4'):{'Enzyme':'AL_F'},
         ('G3P-C2','Fru1_6P2-C5'):{'Enzyme':'AL_F'},
         ('G3P-C3','Fru1_6P2-C6'):{'Enzyme':'AL_F'},
         ('G3P-C1','DHAP-C3'):{'Enzyme':'TIM'},
         ('G3P-C2','DHAP-C2'):{'Enzyme':'TIM'},
         ('G3P-C3','DHAP-C1'):{'Enzyme':'TIM'},
         ('G3P-C1','Fru6P-C4'):{'Enzyme':'TA'},
         ('G3P-C2','Fru6P-C5'):{'Enzyme':'TA'},
         ('G3P-C3','Fru6P-C6'):{'Enzyme':'TA'},
         ('Fru6P-C1','Glc6P-C1'):{'Enzyme':'GPI'},
         ('Fru6P-C2','Glc6P-C2'):{'Enzyme':'GPI'},
         ('Fru6P-C3','Glc6P-C3'):{'Enzyme':'GPI'},
         ('Fru6P-C4','Glc6P-C4'):{'Enzyme':'GPI'},
         ('Fru6P-C5','Glc6P-C5'):{'Enzyme':'GPI'},
         ('Fru6P-C6','Glc6P-C6'):{'Enzyme':'GPI'},
         ('Glc6P-C1','Glc1P-C1'):{'Enzyme':'PM'},
         ('Glc6P-C2','Glc1P-C2'):{'Enzyme':'PM'},
         ('Glc6P-C3','Glc1P-C3'):{'Enzyme':'PM'},
         ('Glc6P-C4','Glc1P-C4'):{'Enzyme':'PM'},
         ('Glc6P-C5','Glc1P-C5'):{'Enzyme':'PM'},
         ('Glc6P-C6','Glc1P-C6'):{'Enzyme':'PM'},
         ('Fru6P-C1','Man6P-C1'):{'Enzyme':'MI'},
         ('Fru6P-C2','Man6P-C2'):{'Enzyme':'MI'},
         ('Fru6P-C3','Man6P-C3'):{'Enzyme':'MI'},
         ('Fru6P-C4','Man6P-C4'):{'Enzyme':'MI'},
         ('Fru6P-C5','Man6P-C5'):{'Enzyme':'MI'},
         ('Fru6P-C6','Man6P-C6'):{'Enzyme':'MI'}
          
}


nx.set_edge_attributes(ctm,attrs)
TK1TK2=[('Xu5P-C3','G3P-C1'),('Xu5P-C4','G3P-C2'),('Xu5P-C5','G3P-C3')]

print('ALGEBRAIC CONNECTIVITY',nx.algebraic_connectivity(ctm))

#%%

#DEPTH FIRST SEARCH

edgeList_Ru5P_C1 = list(nx.edge_dfs(ctm,'Ru5P-C1'))
edgeList_G3P_C1 = list(nx.edge_dfs(ctm,'G3P-C1'))
edgeList_G3P_C2 = list(nx.edge_dfs(ctm,'G3P-C2'))
edgeList_G3P_C3 = list(nx.edge_dfs(ctm,'G3P-C3'))

Ru5P1 = list(dict(nx.dfs_predecessors(ctm,'Ru5P-C1')).keys())
Ru5P1.append('Ru5P-C1')

G3P1 = list(dict(nx.dfs_predecessors(ctm,'G3P-C1')).keys())
G3P1.append('G3P-C1')

G3P2 = list(dict(nx.dfs_predecessors(ctm,'G3P-C2')).keys())
G3P2.append('G3P-C2')

G3P3 = list(dict(nx.dfs_predecessors(ctm,'G3P-C3')).keys())
G3P3.append('G3P-C3')



#%%

####PLOT SUBGRAPHS
pp1 = PdfPages('Ru5P1_ctm.pdf')

Ru5P1_ctm = nx.Graph()

Ru5P1_ctm.add_nodes_from(Ru5P1)
Ru5P1_ctm.add_edges_from(edgeList_Ru5P_C1)

a = plt.figure(figsize=(20,15))

pos = nx.kamada_kawai_layout(ctm)

nx.draw_networkx_nodes(Ru5P1_ctm,pos,nodelist=Ru5P1, node_color='grey', node_size=900)
nx.draw_networkx_edges(Ru5P1_ctm,pos,edgelist=edgeList_Ru5P_C1,edge_color='black', width = 4)

nx.draw_networkx_labels(Ru5P1_ctm,pos,font_size=14,font_color='red', font_weight='bold')

plt.title('Ru5P-C1 network')

plt.savefig('Ru5P1_ctm')

plt.show()

pp1.savefig(a)
pp1.close()



pp2 = PdfPages('G3P1_ctm.pdf')

G3P1_ctm = nx.Graph()

G3P1_ctm.add_nodes_from(G3P1)
G3P1_ctm.add_edges_from(edgeList_G3P_C1)

b = plt.figure(figsize=(20,15))

pos = nx.kamada_kawai_layout(ctm)

nx.draw_networkx_nodes(G3P1_ctm,pos,nodelist=G3P1, node_color='orange', node_size=900)
nx.draw_networkx_edges(G3P1_ctm,pos,edgelist=edgeList_G3P_C1,edge_color='black', width = 4)

nx.draw_networkx_labels(G3P1_ctm,pos,font_size=14,font_color='red', font_weight='bold')

plt.title('G3P-C1 network')

plt.savefig('G3P1_ctm')

plt.show()

pp2.savefig(b)
pp2.close()



pp3 = PdfPages('G3P2_ctm.pdf')

G3P2_ctm = nx.Graph()

G3P2_ctm.add_nodes_from(G3P2)
G3P2_ctm.add_edges_from(edgeList_G3P_C2)

c = plt.figure(figsize=(20,15))

pos = nx.kamada_kawai_layout(ctm)

nx.draw_networkx_nodes(G3P2_ctm,pos,nodelist=G3P2, node_color='blue', node_size=900)
nx.draw_networkx_edges(G3P2_ctm,pos,edgelist=edgeList_G3P_C2,edge_color='black', width = 4)

nx.draw_networkx_labels(G3P2_ctm,pos,font_size=14,font_color='red', font_weight='bold')

plt.title('G3P-C2 network')

plt.savefig('G3P2_ctm')

plt.show()

pp3.savefig(c)
pp3.close()



pp4 = PdfPages('G3P3_ctm.pdf')

G3P3_ctm = nx.Graph()

G3P3_ctm.add_nodes_from(G3P3)
G3P3_ctm.add_edges_from(edgeList_G3P_C3)

d = plt.figure(figsize=(20,15))

pos = nx.kamada_kawai_layout(ctm)

nx.draw_networkx_nodes(G3P3_ctm,pos,nodelist=G3P3, node_color='lime', node_size=900)
nx.draw_networkx_edges(G3P3_ctm,pos,edgelist=edgeList_G3P_C3,edge_color='black', width = 4)

nx.draw_networkx_labels(G3P3_ctm,pos,font_size=14,font_color='red', font_weight='bold')

plt.title('G3P-C3 network')

plt.savefig('G3P3_ctm')

plt.show()

pp4.savefig(d)
pp4.close()




#%%

def CountMetabolites(ctmList):
    temp = []    
    [temp.append(i.split('-')[0]) for i in ctmList]
    from collections import Counter
    return(Counter(temp))

#%%


def countIsotopomers(labelList):
    G3P1List = []
    G3P2List = []
    G3P3List = []
    Ru5P1List = []
    
    CountDict={'Ru5P':0,'Xu5P':0,'Rib5P':0,'G3P':0,'Sed7P':0,
         'Ery4P':0,'Fru6P':0,'DHAP':0,'Sed1_7P2':0,'Fru1_6P2':0,
          'Man6P':0,'Glc6P':0,'Glc1P':0}
    
    for i in labelList:
        if any(i in s for s in Ru5P1):
             Ru5P1List.append('a')
        if any(i in s for s in G3P1):
             G3P1List.append('a')
        if any(i in s for s in G3P2):
             G3P2List.append('a')
        if any(i in s for s in G3P3):
             G3P3List.append('a')
             

    BigList = list([set(Ru5P1List),set(G3P1List),set(G3P2List),set(G3P3List)])
   
    for s in range(len(BigList)):
            if s == 0 and len(BigList[s]) != 0:
                a =CountMetabolites(Ru5P1)
                for t in a:
                    CountDict[t] += a[t]  
                    
            if s == 1 and len(BigList[s]) != 0:
                a =CountMetabolites(G3P1)
                for t in a:
                    CountDict[t] += a[t] 
                    
            if s == 2 and len(BigList[s]) != 0:
                a =CountMetabolites(G3P2)
                for t in a:
                    CountDict[t] += a[t] 
                    
            if s == 3 and len(BigList[s]) != 0:
                a =CountMetabolites(G3P3)
                for t in a:
                    CountDict[t] += a[t] 
    import itertools           
    cnt = 0
    for i in CountDict.values():
        if i != 0:
            cnt += len(list(itertools.product(('0','1'),repeat = i)))
    
    #print(cnt)
    return(CountDict)
        
    
        
#%%

def deleteExperiments(carbontransitionmap,carbonList,delEnz=None,metaboliteIso=False):
    
    ctm2 = carbontransitionmap
        
    Enzy = list(nx.get_edge_attributes(ctm2,'Enzyme').items())
    
    
    edgeList=[]
    a,b = zip(*Enzy)
    for i,j in zip(a,b):
        if j == delEnz:
            edgeList.append(i)
        

    ctm2.remove_edges_from(edgeList)
    
          
    Control = []
          
    for i in carbonList:
        subctm = list(dict(nx.dfs_predecessors(ctm2,i)).keys())
        subctm.append(i)
        Control=Control + subctm
        
    Control = list(set(Control))    
    a = CountMetabolites(Control)
    
    import itertools           
    cnt = 0
    metabolite_iso = []
    for k,l in a.items():
        iso = len(list(itertools.product(('0','1'),repeat = l)))
        cnt += iso
        metabolite_iso.append((k,iso))
        
    if metaboliteIso == True:
        return(metabolite_iso)
        
    else:
        return(cnt)

#%%
    
# ENZYME DELETION EXPERIMENT1: FULLY LABELED RU5P
    
Enzymes = ['RU5PE','RIB5PI','TA','TIM','GPI','PM']

PI = []

for j in Enzymes:
    ctm2 = ctm.copy()
    res = deleteExperiments(ctm2,['Ru5P-C1','Ru5P-C2','Ru5P-C3','Ru5P-C4','Ru5P-C5'],j)
    PI.append(res)
    
print(PI)

ctm3 = ctm.copy()
deleteExperiments(ctm3,['Ru5P-C1','Ru5P-C2','Ru5P-C3','Ru5P-C4','Ru5P-C5'],'TK1')
ctm3.remove_edges_from(TK1TK2)
PI.append(deleteExperiments(ctm3,['Ru5P-C1','Ru5P-C2','Ru5P-C3','Ru5P-C4','Ru5P-C5'],'TK2'))
Enzymes.append('TK')

ctm4 = ctm.copy()
deleteExperiments(ctm4,['Ru5P-C1','Ru5P-C2','Ru5P-C3','Ru5P-C4','Ru5P-C5'],'AL_F')
PI.append(deleteExperiments(ctm4,['Ru5P-C1','Ru5P-C2','Ru5P-C3','Ru5P-C4','Ru5P-C5'],'AL_S'))
Enzymes.append('AL')

sort_PI = []
for i,j in zip(Enzymes,PI):
    sort_PI.append((i,j))

sort_PI = sorted(sort_PI, key=lambda x: x[1])
Enzymes,PI = list(zip(*sort_PI))

PI = 100*np.array(PI)/704

plt.bar(range(len(Enzymes)),PI)
plt.xticks(range(len(Enzymes)),Enzymes, rotation='vertical', fontsize=15)
plt.yticks(fontsize=15)
#plt.xlabel('Enzymes', fontsize=15)
plt.ylabel('percent of all possible Isotopomers', fontsize=15)
#plt.title('FULLY LABELED RU5P')
plt.show()



#%%

# ENZYME DELETION EXPERIMENT2: EFFECTS ON AN GP3-CTM , SART-LABEL RU5P-C3

Enzymes = ['RU5PE','RIB5PI','TA','TIM','GPI','PM']

PI = []

for j in Enzymes:
    ctm2 = ctm.copy()
    res = deleteExperiments(ctm2,['Ru5P-C3'],j)
    PI.append(res)
    
print(PI)

ctm3 = ctm.copy()
deleteExperiments(ctm3,['Ru5P-C3'],'TK1')
ctm3.remove_edges_from(TK1TK2)
PI.append(deleteExperiments(ctm3,['Ru5P-C3'],'TK2'))
Enzymes.append('TK')


ctm4 = ctm.copy()
deleteExperiments(ctm4,['Ru5P-C3'],'AL_F')
PI.append(deleteExperiments(ctm4,['Ru5P-C3'],'AL_S'))
Enzymes.append('AL')

sort_PI = []
for i,j in zip(Enzymes,PI):
    sort_PI.append((i,j))

sort_PI = sorted(sort_PI, key=lambda x: x[1])
Enzymes,PI = list(zip(*sort_PI))


PI = 100*np.array(PI)/30

plt.bar(range(len(Enzymes)),PI)
plt.xticks(range(len(Enzymes)),Enzymes, rotation='vertical', fontsize=15)
plt.yticks(fontsize=15)
#plt.xlabel('Enzymes', fontsize=15)
plt.ylabel('percent of all possible Isotopomers', fontsize=15)
#plt.title('EFFECTS ON AN GP3-CTM , SART-LABEL RU5P-C3')
plt.show()

#%%

# ENZYME DELETION EXPERIMENT3: EFFECTS ON THE RU5P-CTM, START LABELS RU5P-C1 AND C2

Enzymes = ['RU5PE','RIB5PI','TA','TIM','GPI','PM']

PI = []

for j in Enzymes:
    ctm2 = ctm.copy()
    res = deleteExperiments(ctm2,['Ru5P-C1','Ru5P-C2'],j)
    PI.append(res)
    
print(PI)

ctm3 = ctm.copy()
deleteExperiments(ctm3,['Ru5P-C1','Ru5P-C2'],'TK1')
ctm3.remove_edges_from(TK1TK2)
PI.append(deleteExperiments(ctm3,['Ru5P-C1','Ru5P-C2'],'TK2'))
Enzymes.append('TK')

ctm4 = ctm.copy()
deleteExperiments(ctm4,['Ru5P-C1','Ru5P-C2'],'AL_F')
PI.append(deleteExperiments(ctm4,['Ru5P-C1','Ru5P-C2'],'AL_S'))
Enzymes.append('AL')

sort_PI = []
for i,j in zip(Enzymes,PI):
    sort_PI.append((i,j))

sort_PI = sorted(sort_PI, key=lambda x: x[1])
Enzymes,PI = list(zip(*sort_PI))

PI = 100*np.array(PI)/64

plt.bar(range(len(Enzymes)),PI)
plt.xticks(range(len(Enzymes)),Enzymes, rotation='vertical', fontsize=15)
plt.yticks(fontsize=15)
#plt.xlabel('Enzymes', fontsize=15)
plt.ylabel('percent of all possible Isotopomers', fontsize=15)
#plt.title('EFFECTS ON THE RU5P-CTM, START LABELS RU5P-C1 AND C2')
plt.show()


#%%
