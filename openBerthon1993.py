"""
@author: timmy
"""

import modelbase
import modelbase.ratelaws as rl
import numpy as np
import matplotlib.pyplot as plt
from PPPmodels import LabelPPP
from PPPmodels import PPP


################################################################################
#'--------------------------------Fig. 3--------------------------------------'
################################################################################
#
# start concentrations


y0d ={'Ru5P':1e-7,
'Xu5P':1e-7,
'Rib5P':1e-7,
'G3P':1e-7,
'Sed7P':1e-7,
'Ery4P':1e-7,
'Fru6P':5e-6,
'DHAP':5e-6,
'Sed1_7P2':1e-7,
'Fru1_6P2':28.5e-3,
'Man6P':1e-7,
'Glc6P':19e-3,
'Glc1P':1e-7}


PPP_model = PPP()
Test = LabelPPP()

y0 = Test.set_initconc_cpd_labelpos(y0d)
y0[Test.cpdIdDict['Glc6P111111']] = 28.5e-3


Test.par.update(PPP_model.par2) #Berthon parameter

kout1 = 2e-3 #arbitrary values
kout2 = 2e-3
vin = 2e-4
Test.par.update({'vin':vin,'kout1':kout1, 'kout2':kout2})

Test.set_rate('vin',lambda p:p.vin)
Test.set_stoichiometry('vin',{'Ru5P11111':1})

def vout1(p,y):
    return rl.massAction(p.kout1,y)
Test.add_carbonmap_reaction('vout1',vout1,[0,1,2,3,4,5],['Glc6P'],[],'Glc6P')

def vout2(p,y):
    return rl.massAction(p.kout2,y)
Test.add_carbonmap_reaction('vout2',vout1,[0,1,2],['G3P'],[],'G3P')


begin=0
end=50000
steps=10000

T = np.linspace(begin,end,steps)

s = modelbase.Simulator(Test)
s.integrator.linear_solver = 'SPGMR'
s.integrator.atol = 1e-15
s.integrator.rtol = 1e-15

#time simulations

import time 
start_time = time.time()
s.timeCourse(T,y0)
print("--- %s seconds ---" % 
(time.time() - start_time))

# plot 3a

plt.plot(s.getT(),s.getVarByName('Glc6P111111')*1000, label = 'Glc6P111111')
plt.plot(s.getT(),s.getVarByName('Glc6P000000')*1000, label = 'Glc6P000000')
plt.plot(s.getT(),s.getVarByName('Glc6P111000')*1000, label ='Glc6P111000')
plt.xlim(0,50000)
plt.xlabel('Time [s]')
plt.ylabel('Concentration [mM]')
plt.legend()
plt.show()

#plot 3b

plt.plot(s.getT(),s.getVarByName('Glc6P110000')*1000, label = 'Glc6P110000')
plt.plot(s.getT(),s.getVarByName('Glc6P001000')*1000, label = 'Glc6P001000')
plt.plot(s.getT(),s.getVarByName('Glc6P001111')*1000, label ='Glc6P001111')
plt.plot(s.getT(),s.getVarByName('Glc6P000111')*1000, label = 'Glc6P000111')
plt.plot(s.getT(),s.getVarByName('Glc6P110111')*1000, label ='Glc6P110111')
plt.xlim(0,50000)
plt.xlabel('Time [s]')
plt.ylabel('Concentrations [mM]')
plt.legend()
plt.show()

# plot 3c

plt.plot(s.getT(),s.getVarByName('Glc6P100000')*1000+s.getVarByName('Glc6P010000')*1000, label = 'Glc6P100000+Glc6P010000')
plt.plot(s.getT(),s.getVarByName('Glc6P011000')*1000+s.getVarByName('Glc6P101000')*1000, label = 'Glc6P011000 + Glc6P101000')
plt.plot(s.getT(),s.getVarByName('Glc6P011111')*1000+s.getVarByName('Glc6P101111')*1000, label ='Glc6P011111+Glc6P101111')
plt.plot(s.getT(),s.getVarByName('Glc6P100111')*1000+s.getVarByName('Glc6P010111')*1000, label ='Glc6P100111+Glc6P010111')
plt.xlabel('Time [s]')
plt.ylabel('Concentrations [mM]')
plt.xlim(0,50000)
plt.legend()
plt.show()

print('compare with: H. A. Berthon, W. A. Bubb, and P. W. Kuchel. 13C n.m.r. isotopomer and computer-simulation studies of the non-oxidative pentose phosphate pathway of human erythrocytes. Biochemical Journal, 296(2):379–387, Dec. 1993')